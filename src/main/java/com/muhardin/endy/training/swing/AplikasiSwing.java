package com.muhardin.endy.training.swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AplikasiSwing {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("Aplikasi Swing");
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setBackground(new Color(186, 252, 3));

        JPanel panel = new JPanel();
        panel.setBackground(new Color(3, 223, 252));
        JTextField txtPesan = new JTextField(50);
        JButton tombolKirim = new JButton("Kirim");
        panel.add(txtPesan);
        panel.add(tombolKirim);

        // menambahkan event handler ke tombol kirim
        tombolKirim.addActionListener(new TombolKirimListener(txtPesan));
         
        /*
        frame.getContentPane().add(new JButton("Halo1"));
        frame.getContentPane().add(new JButton("Halo2"));
        frame.getContentPane().add(new JButton("Halo3"));
        */
        frame.getContentPane().add(panel);
        frame.getContentPane().setBackground(new Color(186, 252, 3));
        frame.setVisible(true);
    }

    static class TombolKirimListener implements ActionListener {

        private JTextField txtPesan;

        public TombolKirimListener(JTextField txtPesan){
            this.txtPesan = txtPesan;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Tombol kirim ditekan");
            System.out.println("Pesan : "+txtPesan.getText());
            ((JButton) e.getSource()).setText(txtPesan.getText());
            txtPesan.setText("");
        }

    }
}
