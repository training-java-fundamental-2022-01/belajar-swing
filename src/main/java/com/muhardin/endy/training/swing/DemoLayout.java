package com.muhardin.endy.training.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DemoLayout {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setTitle("Demo Layout");
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setLayout(new BorderLayout());

        JPanel panelKiri = new JPanel();
        panelKiri.setBackground(Color.GREEN);
        panelKiri.setLayout(new GridLayout(0,1));
        panelKiri.add(new JButton("Halo33333333333331"));
        panelKiri.add(new JButton("Halo33333333333332"));
        panelKiri.add(new JButton("Halo33333333333333"));

        JPanel panelKanan = new JPanel();
        panelKanan.setBackground(Color.RED);
        panelKanan.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        panelKanan.add(new JButton("Halo4"), gbc);

        JPanel panelBawah = new JPanel();
        panelBawah.setLayout(new FlowLayout(FlowLayout.CENTER));
        panelBawah.setBackground(Color.YELLOW);
        panelBawah.add(new JButton("Halo2"));
        

        frame.getContentPane().add(new JButton("Halo1"), BorderLayout.PAGE_START);
        frame.getContentPane().add(panelBawah, BorderLayout.PAGE_END);
        frame.getContentPane().add(panelKiri, BorderLayout.LINE_START);
        frame.getContentPane().add(panelKanan, BorderLayout.LINE_END);
        frame.getContentPane().add(new JButton("Halo5"), BorderLayout.CENTER);

        frame.setVisible(true);
    }
}
